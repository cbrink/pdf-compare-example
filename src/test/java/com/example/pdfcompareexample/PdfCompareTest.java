package com.example.pdfcompareexample;

import de.redsix.pdfcompare.CompareResult;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class PdfCompareTest {

    @Test
    public void givenIdenticalFiles_whenCompare_thenExpectMatch() throws IOException {
        String filename1 = "applicant1.pdf";
        String filename2 = "applicant1copy.pdf";

        PdfCompare compare = new PdfCompare();
        CompareResult result = compare.compare(filename1, filename2);
        Assert.assertTrue("expected match", result.isEqual());
        Assert.assertFalse("expected no match", result.hasDifferenceInExclusion());
        Assert.assertEquals("expected match", 7, result.getNumberOfPages());
    }

    @Test
    public void givenTextMatchNonIdenticalFiles_whenCompare_thenExpectNotEqual() throws IOException {
        String filename1 = "applicant1.pdf";
        String filename2 = "applicant1a.pdf";

        PdfCompare compare = new PdfCompare();
        CompareResult result = compare.compare(filename1, filename2);
        Assert.assertFalse("expected match", result.isEqual());
        Assert.assertFalse("expected match", result.hasDifferenceInExclusion());
        Assert.assertEquals("expected no match", 7, result.getNumberOfPages());
    }

    @Test
    public void givenNonIdenticalFiles_whenCompare_thenExpectNotEqual() throws IOException {
        String filename1 = "applicant1.pdf";
        String filename2 = "applicant2.pdf";

        PdfCompare compare = new PdfCompare();
        CompareResult result = compare.compare(filename1, filename2);
        Assert.assertFalse("expected match", result.isEqual());
        Assert.assertFalse("expected match", result.hasDifferenceInExclusion());
        Assert.assertEquals("expected no match", 7, result.getNumberOfPages());
    }

}