package com.example.pdfcompareexample;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class PdfClownCompareTest {
    @Test
    public void givenIdenticalFiles_whenCompare_thenExpectMatch() throws IOException {
        String filename1 = "applicant1.pdf";
        String filename2 = "applicant1copy.pdf";

        PdfClownCompare compare = new PdfClownCompare();
        int result = compare.compare(filename1, filename2);
        Assert.assertEquals("expected match", 0, result);
    }

    @Test
    public void givenTextMatchNonIdenticalFiles_whenCompare_thenExpectNotEqual() throws IOException {
        String filename1 = "applicant1.pdf";
        String filename2 = "applicant1a.pdf";

        PdfClownCompare compare = new PdfClownCompare();
        int result = compare.compare(filename1, filename2);
        Assert.assertEquals("expected no match", 9, result);
    }

    @Test
    public void givenNonIdenticalFiles_whenCompare_thenExpectNotEqual() throws IOException {
        String filename1 = "applicant1.pdf";
        String filename2 = "applicant2.pdf";

        PdfClownCompare compare = new PdfClownCompare();
        int result = compare.compare(filename1, filename2);
        Assert.assertEquals("expected no match", 9, result);
    }

}