package com.example.pdfcompareexample;

import de.redsix.pdfcompare.CompareResult;
import de.redsix.pdfcompare.PdfComparator;

import java.io.IOException;

public class PdfCompare {
    public CompareResult compare(final String filename1, final String filename2) throws IOException {
        final CompareResult result = new PdfComparator(filename1, filename2).compare();
        return result;
    }
}
