package com.example.pdfcompareexample;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Comparator;

public class PdfBoxCompare implements Comparator<PDDocument> {
    public int compare(final String filename1, final String filename2) throws IOException {
        File file1 = new File(filename1);
        File file2 = new File(filename2);

        PDDocument document1 = PDDocument.load(file1);
        PDDocument document2 = PDDocument.load(file2);
        return compare(document1, document2);
    }

    @Override
    public int compare(PDDocument d1, PDDocument d2) {
        boolean match = false;
        if (d1.getNumberOfPages() != d2.getNumberOfPages()) {
            return d1.getNumberOfPages() - d2.getNumberOfPages();
        }
        PDFTextStripper textStripper = null;
        try {
            textStripper = new PDFTextStripper();
            String text1 = textStripper.getText(d1);
            String text2 = textStripper.getText(d2);
            if (!text1.equals(text2)) {
                return text1.compareTo(text2);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return -999;
        }

        PDFRenderer renderer1 = new PDFRenderer(d1);
        PDFRenderer renderer2 = new PDFRenderer(d2);
        try {
            for (int i = 0; i < d1.getNumberOfPages(); i++) {
                BufferedImage image1 = renderer1.renderImage(i);
                BufferedImage image2 = renderer2.renderImage(i);
                if (!bufferedImagesEqual(image1, image2)) {
                    return 999;
                }
            }
            return 0;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -999;
    }

    boolean bufferedImagesEqual(BufferedImage img1, BufferedImage img2) {
        if (img1.getWidth() == img2.getWidth() && img1.getHeight() == img2.getHeight()) {
            for (int x = 0; x < img1.getWidth(); x++) {
                for (int y = 0; y < img1.getHeight(); y++) {
                    if (img1.getRGB(x, y) != img2.getRGB(x, y))
                        return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }
}
