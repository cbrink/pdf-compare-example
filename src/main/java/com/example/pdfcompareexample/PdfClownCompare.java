package com.example.pdfcompareexample;

import org.pdfclown.bytes.IBuffer;
import org.pdfclown.documents.Document;
import org.pdfclown.documents.Page;
import org.pdfclown.documents.contents.ContentScanner;
import org.pdfclown.documents.contents.composition.PrimitiveComposer;
import org.pdfclown.documents.contents.objects.CompositeObject;
import org.pdfclown.documents.contents.objects.ContainerObject;
import org.pdfclown.documents.contents.objects.ContentObject;
import org.pdfclown.documents.contents.objects.Operation;
import org.pdfclown.documents.contents.objects.ShowText;
import org.pdfclown.documents.contents.objects.Text;
import org.pdfclown.documents.interchange.metadata.Metadata;
import org.pdfclown.files.File;
import org.pdfclown.objects.PdfArray;
import org.pdfclown.objects.PdfDataObject;
import org.pdfclown.objects.PdfDirectObject;
import org.pdfclown.objects.PdfStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PdfClownCompare implements Comparator<Document> {
    public int compare(final String filename1, final String filename2) throws IOException {

        try (File file1 = new File(filename1); File file2 = new File(filename2);) {
            Document document1 = file1.getDocument();
            Document document2 = file2.getDocument();
            return compare(document1, document2);
        }
    }

    @Override
    public int compare(Document d1, Document d2) {
        if (d1.getNumberOfPages() != d2.getNumberOfPages()) {
            return d1.getNumberOfPages() - d2.getNumberOfPages();
        }
        Metadata metadata = d1.getMetadata();
        if(metadata.exists()) {
            try {
                org.w3c.dom.Document metadataContent = metadata.getContent();
                System.out.println("Metadata"+toString(metadataContent));
            } catch (Exception e) {
                System.out.println("Metadata extraction failed: " + e.getMessage());
            }
        }
        List<String> streams1 = new ArrayList<>();
        for(Page page : d1.getPages()) {
            PrimitiveComposer composer = new PrimitiveComposer(page);
            ContentScanner scanner = composer.getScanner();
            while (scanner.moveNext()) {
                ContentObject content = scanner.getCurrent();
                if (content instanceof Text || content instanceof ContainerObject) {
                    scanner = scanner.getChildLevel();
                    continue;
                } else if (content instanceof ShowText) {
                    System.out.println("ShowText:"+ new String(((ShowText)content).getText()));
                };
            }
            printContentObjects(page.getContents(),0,0);
            List<PdfDataObject> streamObjects = new ArrayList<>();
            PdfDataObject contentsDataObject = page.getContents().getBaseDataObject();
            if(contentsDataObject instanceof PdfArray)
            {streamObjects.addAll((List<PdfDirectObject>)contentsDataObject);}
            else
            {streamObjects.add(contentsDataObject);}

            int streamIndex = -1;
            for(PdfDataObject streamObject : streamObjects)
            {
                PdfStream stream = (PdfStream)streamObject.resolve();
                IBuffer streamBody = stream.getBody();
                streams1.add(streamBody.getString(0, (int)streamBody.getLength()));
            }
        }
        List<String> streams2 = new ArrayList<>();
        for(Page page : d2.getPages()) {
            List<PdfDataObject> streamObjects = new ArrayList<>();
            PdfDataObject contentsDataObject = page.getContents().getBaseDataObject();
            if(contentsDataObject instanceof PdfArray)
            {streamObjects.addAll((List<PdfDirectObject>)contentsDataObject);}
            else
            {streamObjects.add(contentsDataObject);}

            int streamIndex = -1;
            for(PdfDataObject streamObject : streamObjects)
            {
                PdfStream stream = (PdfStream)streamObject.resolve();
                IBuffer streamBody = stream.getBody();
                streams2.add(streamBody.getString(0, (int)streamBody.getLength()));
            }
        }
        streams1.removeAll(streams2);
        return streams1.size();
    }

    private int printContentObjects(
            List<ContentObject> objects,
            int index,
            int level
    )
    {
        String indentation = getIndentation(level);
        for(ContentObject object : objects)
        {
      /*
        NOTE: Contents are expressed through both simple operations and composite objects.
      */
            if(object instanceof Operation)
            {System.out.println("   " + indentation + (++index) + ": " + object);}
            else if(object instanceof CompositeObject)
            {
                System.out.println(
                        "   " + indentation + object.getClass().getSimpleName()
                                + "\n   " + indentation + "{"
                );
                index = printContentObjects(((CompositeObject)object).getObjects(),index,level+1);
                System.out.println("   " + indentation + "}");
            }
            if(index > 9)
                break;
        }
        return index;
    }

    protected String getIndentation(
            int level
    )
    {
        StringBuilder indentationBuilder = new StringBuilder();
        for(int i = 0; i < level; i++)
        {indentationBuilder.append(' ');}
        return indentationBuilder.toString();
    }

    private String toString(
            org.w3c.dom.Document document
    )
    {
        try
        {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(document), new StreamResult(writer));
            return writer.getBuffer().toString();
        }
        catch(Exception e)
        {
            System.out.println("Metadata content extraction failed: " + e.getMessage());
            return "";
        }
    }
}
